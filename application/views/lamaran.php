<div class="container">
  <div class="container-fluid">
    <form method="POST" action="">
      <div class="form-group">
        <label for="exampleInputEmail1">Posisi yang dilamar</label>
        <input type="text" class="form-control" id="exampleInputEmail1" name="position" value="<?php echo $user[0]['position']?>" readonly>
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Nama</label>
        <input type="text" class="form-control" id="exampleInputEmail1" name="nama" value="<?php echo $user[0]['name']?>" readonly>
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">No KTP</label>
        <input type="number" class="form-control" id="exampleInputEmail1" name="no_ktp" value="<?php echo $user[0]['no_ktp']?>" readonly>
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Tempat Tanggal Lahir</label>
        <input type="text" class="form-control" id="exampleInputEmail1" name="ttl" value="<?php echo $user[0]['ttl']?>" readonly>
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Jenis Kelamin</label>
        <input type="text" class="form-control" id="exampleInputEmail1" name="jk" value="<?php echo $user[0]['jk']?>" readonly>
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Agama</label>
        <input type="text" class="form-control" id="exampleInputEmail1" name="agama" value="<?php echo $user[0]['agama']?>" readonly>
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Golongan Darah</label>
        <input type="text" class="form-control" id="exampleInputEmail1" name="goldar" value="<?php echo $user[0]['goldar']?>" readonly>
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Status</label>
        <input type="text" class="form-control" id="exampleInputEmail1" name="status" value="<?php echo $user[0]['status']?>" readonly>
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Alamat KTP</label>
        <input type="text" class="form-control" id="exampleInputEmail1" name="alamat_ktp" value="<?php echo $user[0]['alamat_ktp']?>" readonly>
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Alamat Tinggal</label>
        <input type="text" class="form-control" id="exampleInputEmail1" name="alamat_domisili" value="<?php echo $user[0]['alamat_domisili']?>" readonly>
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Email</label>
        <input type="email" class="form-control" id="exampleInputEmail1" name="email" value="<?php echo $user[0]['email']?>" readonly>
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">No telp</label>
        <input type="number" class="form-control" id="exampleInputEmail1" name="no_telp" value="<?php echo $user[0]['no_telp']?>" readonly>
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Orang Terdekat yang bisa dihubungi</label>
        <input type="text" class="form-control" id="exampleInputEmail1" name="kerabat" value="<?php echo $user[0]['kerabat']?>" readonly>
      </div>
      <div class="form-group">
        <div class="edu">
          <label for="exampleInputEmail1">Pendidikan</label>
          <button class="add_form_field_edu">Add New &nbsp; 
            <span style="font-size:16px; font-weight:bold;">+ </span>
          </button>
          <div>
            <table>
              <tr>
                <td><input type="text" class="form-control" name="jenjang[]" placeholder="Jenjang" required></td>
                <td><input type="text" class="form-control" name="institut[]" placeholder="institut" required></td>
                <td><input type="text" class="form-control" name="jurusan[]" placeholder="Jurusan" required></td>
                <td><input type="text" class="form-control" name="tahunlulus[]" placeholder="Tahun Lulus" required></td>
                <td><input type="text" class="form-control" name="ipk[]" placeholder="IPK" required></td>
              </tr>
            </table>
          </div>  
        </div>
      </div>
      <div class="form-group">
        <div class="kur">
          <label for="exampleInputEmail1">Riwayat Pelatihan</label>
          <button class="add_form_field_kur">Add New &nbsp; 
            <span style="font-size:16px; font-weight:bold;">+ </span>
          </button>
          <div>
            <table>
              <tr>
                <td><input type="text" class="form-control" name="kursus[]" placeholder="Sertifikasi" required></td>
                <td><input type="text" class="form-control" name="sertifikat[]" placeholder="Ada Sertifikat / Tidak" required></td>
                <td><input type="text" class="form-control" name="tahun[]" placeholder="Tahun" required></td>
              </tr>
            </table>
          </div>  
        </div>
      </div>
      <div class="form-group">
        <div class="work">
          <label for="exampleInputEmail1">Riwayat Pekerjaan</label>
          <button class="add_form_field_work">Add New &nbsp; 
            <span style="font-size:16px; font-weight:bold;">+ </span>
          </button>
          <div>
            <table>
              <tr>
                <td><input type="text" class="form-control" name="office[]" placeholder="Tahun" required=""></td>  
                <td><input type="text" class="form-control" name="last_position[]" placeholder="Sertifikasi" required=""></td>
                <td><input type="text" class="form-control" name="salary[]" placeholder="Ada Sertifikat / Tidak" required=""></td>
                <td><input type="text" class="form-control" name="year[]" placeholder="Tahun" required=""></td>
              </tr>
            </table>
          </div>  
        </div>
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Skill</label>
        <input type="text" class="form-control" id="exampleInputEmail1" name="skill" value="<?php echo $user[0]['skill']?>" readonly>
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Bersedia ditempatkan di seluruh Kantor perusahaan</label>
        <input type="text" class="form-control" id="exampleInputEmail1" name="available" value="<?php echo $user[0]['available']?>" readonly>
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Penghasilan yg diharapkan</label>
        <input type="number" class="form-control" id="exampleInputEmail1" name="salary" value="<?php echo $user[0]['salary']?>" readonly>
      </div>
    </form>
  </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>



<script type="text/javascript">
  $(document).ready(function() {
    var max_fields = 10;
    var wrapper = $(".edu");
    var add_button = $(".add_form_field_edu");

    var x = 1;
    $(add_button).click(function(e) {
      e.preventDefault();
      if (x < max_fields) {
        x++;
            $(wrapper).append('<div><label for="exampleInputEmail1">Pendidikan</label><table><tr><td><input type="text" class="form-control" name="jenjang[]" placeholder="Jenjang" required></td><td><input type="text" class="form-control" name="institut[]" placeholder="institut" required></td><td><input type="text" class="form-control" name="jurusan[]" placeholder="Jurusan" required></td><td><input type="text" class="form-control" name="tahunlulus[]" placeholder="Tahun Lulus" required></td><td><input type="text" class="form-control" name="ipk[]" placeholder="IPK" required></td></tr></table><a href="#" class="delete">Delete</a></div>'); //add input box
          } else {
            alert('You Reached the limits')
          }
        });

    $(wrapper).on("click", ".delete", function(e) {
      e.preventDefault();
      $(this).parent('div').remove();
      x--;
    })
  });

  $(document).ready(function() {
    var max_fields = 10;
    var wrapper = $(".kur");
    var add_button = $(".add_form_field_kur");

    var x = 1;
    $(add_button).click(function(e) {
      e.preventDefault();
      if (x < max_fields) {
        x++;
            $(wrapper).append('<div><label for="exampleInputEmail1">Riwayat Pelatihan</label><table><tr><td><input type="text" class="form-control" name="kursus[]" placeholder="Sertifikasi" required></td><td><input type="text" class="form-control" name="sertifikat[]" placeholder="Ada Sertifikat / Tidak" required></td><td><input type="text" class="form-control" name="tahun[]" placeholder="Tahun" required></td></tr></table><a href="#" class="deletekur">Delete</a></div>'); //add input box
          } else {
            alert('You Reached the limits')
          }
        });

    $(wrapper).on("click", ".deletekur", function(e) {
      e.preventDefault();
      $(this).parent('div').remove();
      x--;
    })
  });

  $(document).ready(function() {
    var max_fields = 10;
    var wrapper = $(".work");
    var add_button = $(".add_form_field_work");

    var x = 1;
    $(add_button).click(function(e) {
      e.preventDefault();
      if (x < max_fields) {
        x++;
            $(wrapper).append('<div><label for="exampleInputEmail1">Riwayat Pekerjaan</label><table> <tr><td><input type="text" class="form-control" name="office[]" placeholder="Nama Perusahaan" required=""></td>  <td><input type="text" class="form-control" name="last_position[]" placeholder="Sertifikasi" required=""></td><td><input type="text" class="form-control" name="salary[]" placeholder="Ada Sertifikat / Tidak" required=""></td><td><input type="text" class="form-control" name="year[]" placeholder="Tahun" required=""></td></tr></table><a href="#" class="deletework">Delete</a></div>'); //add input box
          } else {
            alert('You Reached the limits')
          }
        });

    $(wrapper).on("click", ".deletework", function(e) {
      e.preventDefault();
      $(this).parent('div').remove();
      x--;
    })
  });
</script>


