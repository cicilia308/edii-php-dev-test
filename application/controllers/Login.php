<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model("user_model");
		$this->load->library('form_validation');
	}

	public function index()
	{
		$this->load->view('login');
	}

	public function signup()
	{
		$this->load->view('signup');
	}

	function adduser(){
		$email = $this->input->post('email');
		$pass = $this->input->post('pass');
		$username = $this->input->post('username');

		$data = array(
			'email' => $email,
			'pass' => md5($pass),
			'username' => $username,
			'user_type' => 2
		);
		$this->user_model->add_user($data,'m_user');
		redirect('login');
	}

	function do_login(){
		$email = $this->input->post('email');
		$pass = $this->input->post('pass');
		$where = array(
			'email' => $email,
			'pass' => md5($pass)
		);
		$cek = $this->user_model->cek_login("m_user",$where)->num_rows();
		$a = $this->user_model->cek_login("m_user",$where)->result_array();
		if($cek > 0){

			$data_session = array(
				'id_user' => $a[0]['id'],
				'nama' => $email,
				'status' => "login",
				'user_type' => $a[0]['user_type']
			);

			print_r($data_session);

			$this->session->set_userdata($data_session);

			redirect(base_url("admin"));

		}else{
			echo "Username dan password salah !";
		}
	}

	function logout(){
		$this->session->sess_destroy();
		redirect(base_url('login'));
	}


}
