<?php 

class Admin extends CI_Controller{

	function __construct(){
		parent::__construct();

		$this->load->model("admin_model");

		if($this->session->userdata('status') != "login"){
			redirect(base_url("login"));
		}
	}

	function index(){
		$this->load->view('v_header');
		$this->load->view('admin');
		$this->load->view('v_footer');
	}

	public function addlamaran()
	{
		$this->load->view('v_header');
		$this->load->view('form_lamar');
		$this->load->view('v_footer');
	}

	public function lamaran()
	{
		if ($this->session->userdata('user_type')==1) {
			$id = $this->uri->segment(3);
		} else {
			$id = $this->session->userdata('id_user');
		}	
		$where = array('id' => $id);
		$data['user'] = $this->admin_model->edit_data($where,'t_pelamar')->result_array();
		$data['edu'] = $this->admin_model->edit_data($where,'t_pendidikan')->result();
		$data['kur'] = $this->admin_model->edit_data($where,'t_kursus')->result_array();
		
		// print_r($data);
		// exit(); 	
		$this->load->view('v_header');
		$this->load->view('lamaran', $data);
		$this->load->view('v_footer');
	}

	public function list()
	{
		$data['list'] = $this->admin_model->get_list()->result_array();

		$this->load->view('v_header');
		$this->load->view('list',$data);
		$this->load->view('v_footer');
	}

	public function listuser()
	{
		$data['user'] = $this->admin_model->get_user()->result_array();

		$this->load->view('v_header');
		$this->load->view('userlist',$data);
		$this->load->view('v_footer');
	}


	public function addedlamaran(){

		$a = $this->input->post();

		// print_r($a);
		// exit(); 

		$data = array(
			'id_user' => $this->session->userdata('id_user'),
			'position' => $this->input->post('position'),
			'name' => $this->input->post('nama'),
			'no_ktp' => $this->input->post('no_ktp'),
			'ttl' => $this->input->post('ttl'),
			'jk' => $this->input->post('jk'),
			'agama' => $this->input->post('agama'),
			'goldar' => $this->input->post('goldar'),
			'status' => $this->input->post('status'),
			'alamat_ktp' => $this->input->post('alamat_ktp'),
			'alamat_domisili' => $this->input->post('alamat_domisili'),
			'email' => $this->input->post('email'),
			'kerabat' => $this->input->post('kerabat'),
			'skill' => $this->input->post('skill'),
			'available' => $this->input->post('available'),
			'salary' => $this->input->post('salary')
		);
		$this->admin_model->insertlamaran($data,'t_pelamar');

		$b = $this->input->post('jenjang');
		$c = $this->input->post('institut');
		$d = $this->input->post('jurusan');
		$e = $this->input->post('tahunlulus');
		$f = $this->input->post('ipk');

		for ($i=0; $i < count($b); $i++) { 
			$data = array('id_user' => $this->session->userdata('id_user'),
				'jenjang' => $b[$i],
				'institut' => $c[$i],
				'jurusan' => $d[$i],
				'tahunlulus' => $e[$i],
				'ipk' => $f[$i]);
			$this->admin_model->insertpendidikan($data,'t_pendidikan');
		}

		$kursus = $this->input->post('kursus');
		$sertifikat = $this->input->post('sertifikat');
		$tahun = $this->input->post('tahun');

		for ($i=0; $i < count($kursus); $i++) { 
			$data = array('id_user' => $this->session->userdata('id_user'),
				'kursus' => $kursus[$i],
				'sertifikat' => $sertifikat[$i],
				'tahun' => $tahun[$i]);
			$this->admin_model->insertkursus($data,'t_kursus');
		}

		$office = $this->input->post('office');
		$last_position = $this->input->post('last_position');
		$salary = $this->input->post('salary');
		$year = $this->input->post('year');

		for ($i=0; $i < count($kursus); $i++) { 
			$data = array('id_user' => $this->session->userdata('id_user'),
				'office' => $office[$i],
				'last_position' => $last_position[$i],
				'salary' => $salary[$i],
				'year' => $year[$i],
			);
			$this->admin_model->insertkursus($data,'t_riwayat');
		}
		redirect('Admin');
	}


}