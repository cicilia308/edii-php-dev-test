<?php

class Admin_model extends CI_Model
{

	function insertlamaran($data,$table){
		$this->db->insert($table,$data);
	}

	function insertpendidikan($data,$table){
		$this->db->insert($table,$data);
	}

	function insertkursus($data,$table){
		$this->db->insert($table,$data);
	}

	function get_list(){
		return $this->db->get('t_pelamar');
	}

	function get_user(){
		return $this->db->get('m_user');
	}


	function edit_data($where,$table){		
		return $this->db->get_where($table,$where);
	}



}