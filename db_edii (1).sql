-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 20, 2020 at 02:36 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_edii`
--

-- --------------------------------------------------------

--
-- Table structure for table `m_user`
--

CREATE TABLE `m_user` (
  `id` int(11) NOT NULL,
  `email` varchar(110) NOT NULL,
  `username` varchar(255) NOT NULL,
  `pass` varchar(110) NOT NULL,
  `user_type` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_user`
--

INSERT INTO `m_user` (`id`, `email`, `username`, `pass`, `user_type`) VALUES
(1, 'cicilia308@gmail.com', 'Cicilia Marindi Putri', '82682943a05de360abb183236c632bd6', 1),
(2, 'painah@gmail.com', 'Painah', '202cb962ac59075b964b07152d234b70', 2),
(3, 'sri@gmail.com', 'Sri Mulyani', '202cb962ac59075b964b07152d234b70', 2),
(4, 'paijem@gmail.com', 'paijem', '68053af2923e00204c3ca7c6a3150cf7', 2);

-- --------------------------------------------------------

--
-- Table structure for table `t_kursus`
--

CREATE TABLE `t_kursus` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `kursus` varchar(255) NOT NULL,
  `sertifikat` varchar(255) NOT NULL,
  `tahun` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `t_pelamar`
--

CREATE TABLE `t_pelamar` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `position` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `no_ktp` varchar(255) DEFAULT NULL,
  `ttl` varchar(255) DEFAULT NULL,
  `jk` varchar(200) NOT NULL,
  `agama` varchar(255) DEFAULT NULL,
  `goldar` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `alamat_ktp` varchar(255) DEFAULT NULL,
  `alamat_domisili` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `no_telp` varchar(255) DEFAULT NULL,
  `kerabat` varchar(255) DEFAULT NULL,
  `skill` varchar(255) DEFAULT NULL,
  `available` varchar(255) DEFAULT NULL,
  `salary` varchar(255) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `t_pendidikan`
--

CREATE TABLE `t_pendidikan` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `jenjang` varchar(255) DEFAULT NULL,
  `institut` varchar(255) DEFAULT NULL,
  `jurusan` varchar(255) DEFAULT NULL,
  `tahunlulus` varchar(10) DEFAULT NULL,
  `ipk` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `t_riwayat`
--

CREATE TABLE `t_riwayat` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `office` varchar(255) DEFAULT NULL,
  `last_position` varchar(255) DEFAULT NULL,
  `salary` varchar(255) DEFAULT NULL,
  `year` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `m_user`
--
ALTER TABLE `m_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_kursus`
--
ALTER TABLE `t_kursus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_pelamar`
--
ALTER TABLE `t_pelamar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_pendidikan`
--
ALTER TABLE `t_pendidikan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_riwayat`
--
ALTER TABLE `t_riwayat`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `m_user`
--
ALTER TABLE `m_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `t_kursus`
--
ALTER TABLE `t_kursus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t_pelamar`
--
ALTER TABLE `t_pelamar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t_pendidikan`
--
ALTER TABLE `t_pendidikan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t_riwayat`
--
ALTER TABLE `t_riwayat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
